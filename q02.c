#include <stdio.h>
void main()
{
    int num1, reminder;

    printf("Input number: ");
    scanf("%d", &num1);
    reminder = num1 % 2;
    if (reminder == 0)
        printf("%d is an even number\n", num1);
    else
        printf("%d is an odd number\n", num1);
}
